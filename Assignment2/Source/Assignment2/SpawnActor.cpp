// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnActor.h"

// Sets default values
ASpawnActor::ASpawnActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
		
	//set default values
	SpawnInterval = 3;
	StartTime = 3;
	SpawnCountdown = 0;
	MaxNumberBigAgents = 9;
	MaxNumberSmallAgents = 11;
	AgentCounter = 0;

}

// Called when the game starts or when spawned
void ASpawnActor::BeginPlay()
{
	Super::BeginPlay();

	//set timer
	GetWorldTimerManager().SetTimer(StartTimerHandle, this, &ASpawnActor::AdvancedTimer, 1.0f, true);
	
}

// Called every frame
void ASpawnActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
		
	//if agent should be spawned
	if (StartSpawning)
	{
		SpawnCountdown -= DeltaTime;
		if (SpawnCountdown <= 0)
		{
			//spawning agents
			if (AgentCounter < MaxNumberBigAgents)
			{
				ABigAgentActor* tempReference1 = GetWorld()->SpawnActor<ABigAgentActor>(SpawnObject1, FVector(1230, 1800, 20), FRotator::ZeroRotator);
			}
			if (AgentCounter < MaxNumberSmallAgents)
			{
				ASmallAgentActor* tempReference2 = GetWorld()->SpawnActor<ASmallAgentActor>(SpawnObject2, FVector(2340, 650, 30), FRotator::ZeroRotator);
			}
			SpawnCountdown = SpawnInterval;
			AgentCounter++;
		}
	}

	//spawn player once all agents are spawned
	if (AgentCounter > MaxNumberSmallAgents && AgentCounter > MaxNumberBigAgents && !(PlayerSpawned))
	{
		AAssignment2Character* tempReference3 = GetWorld()->SpawnActor< AAssignment2Character>(SpawnObject3, FVector(-351, 141, 235), FRotator::ZeroRotator);
		PlayerSpawned = true;
	}

}

void ASpawnActor::AdvancedTimer()
{
	//timer
	--StartTime;
	if (StartTime < 1)
	{
		GetWorldTimerManager().ClearTimer(StartTimerHandle);
		CountdownHasFinished();
	}
}

void ASpawnActor::CountdownHasFinished()
{
	StartSpawning = true;
}
