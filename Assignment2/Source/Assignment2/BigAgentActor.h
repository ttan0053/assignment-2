// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Materials/Material.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "BigAgentActor.generated.h"

UCLASS()
class ASSIGNMENT2_API ABigAgentActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABigAgentActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UStaticMeshComponent* SphereVisual;
	
	UPROPERTY(VisibleAnywhere)
		UArrowComponent* ForwardArrow;

	UMaterial* StoredMaterial;

	UMaterialInstanceDynamic* DynamicMaterialInst;

	int32 TargetCheckpoint;

	

	UPROPERTY(EditAnywhere)
		TArray<FVector> Checkpoints;

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
};
