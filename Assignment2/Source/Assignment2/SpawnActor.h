// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/TextRenderComponent.h"
#include "BigAgentActor.h"
#include "SmallAgentActor.h"
#include "Assignment2Character.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnActor.generated.h"

UCLASS()
class ASSIGNMENT2_API ASpawnActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		float SpawnInterval;

	UPROPERTY(EditAnywhere)
		int32 StartTime;

	UPROPERTY(EditAnywhere)
		int MaxNumberBigAgents;

	UPROPERTY(EditAnywhere)
		int MaxNumberSmallAgents;

	UPROPERTY(EditAnywhere, Category = "SpawnObject")
		TSubclassOf<AActor> SpawnObject1;

	UPROPERTY(EditAnywhere, Category = "SpawnObject")
		TSubclassOf<AActor> SpawnObject2;

	UPROPERTY(EditAnywhere, Category = "SpawnObject")
		TSubclassOf<AActor> SpawnObject3;

	bool StartSpawning = false;
	bool PlayerSpawned = false;
	float SpawnCountdown;
	int AgentCounter;

	void AdvancedTimer();

	void CountdownHasFinished();

	FTimerHandle StartTimerHandle;


};
