// Fill out your copyright notice in the Description page of Project Settings.


#include "SmallAgentActor.h"

// Sets default values
ASmallAgentActor::ASmallAgentActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//root component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
	SphereVisual->SetupAttachment(RootComponent);

	//static mesh visual
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));

	if (SphereVisualAsset.Succeeded())
	{
		SphereVisual->SetStaticMesh(SphereVisualAsset.Object);
	}
	SphereVisual->SetWorldScale3D(FVector(0.5, 0.5, 0.5));
	SphereVisual->SetRelativeScale3D(FVector(0.5, 0.5, 0.5));

	//arrow component
	ForwardArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	ForwardArrow->SetupAttachment(RootComponent);
	ForwardArrow->bHiddenInGame = false;

	//set material
	static ConstructorHelpers::FObjectFinder<UMaterial>FoundMaterial(TEXT("/Game/StarterContent/Materials/M_Metal_Gold.M_Metal_Gold"));

	if (FoundMaterial.Succeeded())
	{
		StoredMaterial = FoundMaterial.Object;
	}
	DynamicMaterialInst = UMaterialInstanceDynamic::Create(StoredMaterial, SphereVisual);

	SphereVisual->SetMaterial(0, DynamicMaterialInst);

	//collision
	SphereVisual->SetNotifyRigidBodyCollision(true);
	SphereVisual->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	SphereVisual->OnComponentHit.AddDynamic(this, &ASmallAgentActor::OnHit);

	TargetCheckpoint = 0;

}

// Called when the game starts or when spawned
void ASmallAgentActor::BeginPlay()
{
	Super::BeginPlay();
	
	//manual addition of checkpoints
	Checkpoints.Add(FVector(2340, 650, 170));
	Checkpoints.Add(FVector(1930, 870, 170));
	Checkpoints.Add(FVector(1620, 1110, 170));
	Checkpoints.Add(FVector(1810, 1310, 170));
	Checkpoints.Add(FVector(1650, 1650, 170));
	Checkpoints.Add(FVector(1930, 1810, 170));
	Checkpoints.Add(FVector(2230, 1430, 170));
	Checkpoints.Add(FVector(2060, 610, 170));
	Checkpoints.Add(FVector(2240, -120, 170));
	Checkpoints.Add(FVector(2680, 210, 170));
}

// Called every frame
void ASmallAgentActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//change watch direction to next location
	FVector WatchDirection = Checkpoints[TargetCheckpoint] - this->GetActorLocation();
	WatchDirection.Z = 0;
	WatchDirection.Normalize();

	FVector Forward = FVector(1, 0, 0);

	float Dot = FVector::DotProduct(Forward, WatchDirection);
	float Det = Forward.X * WatchDirection.Y + Forward.Y * WatchDirection.X;
	float Rad = FMath::Atan2(Det, Dot);
	float Degrees = FMath::RadiansToDegrees(Rad);

	this->SetActorRotation(FRotator(0, Degrees, 180));

	//move/lerp to next location
	RootComponent->SetWorldLocation(FMath::Lerp(this->GetActorLocation(), Checkpoints[TargetCheckpoint], DeltaTime * 4));

	if (FVector::Dist(this->GetActorLocation(), Checkpoints[TargetCheckpoint]) < 2.0f)
	{
		TargetCheckpoint++;
		if (TargetCheckpoint > 9)
			TargetCheckpoint = 0;
	}

}

void ASmallAgentActor::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		//reset level
		UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
	}
}

