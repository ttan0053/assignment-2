// Fill out your copyright notice in the Description page of Project Settings.


#include "BigAgentActor.h"

// Sets default values
ABigAgentActor::ABigAgentActor()
{
	PrimaryActorTick.bCanEverTick = true;

	//root component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
	SphereVisual->SetupAttachment(RootComponent);

	//static mesh visual
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));

	if (SphereVisualAsset.Succeeded())
	{
		SphereVisual->SetStaticMesh(SphereVisualAsset.Object);
	}
	SphereVisual->SetWorldScale3D(FVector(1.5, 1.5, 1.5));
	SphereVisual->SetRelativeScale3D(FVector(1.5, 1.5, 1.5));

	//arrow component
	ForwardArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	ForwardArrow->SetupAttachment(RootComponent);
	ForwardArrow->bHiddenInGame = false;

	//set material
	static ConstructorHelpers::FObjectFinder<UMaterial>FoundMaterial(TEXT("/Game/StarterContent/Materials/M_Metal_Copper.M_Metal_Copper"));

	if (FoundMaterial.Succeeded())
	{
		StoredMaterial = FoundMaterial.Object;
	}
	DynamicMaterialInst = UMaterialInstanceDynamic::Create(StoredMaterial, SphereVisual);

	SphereVisual->SetMaterial(0, DynamicMaterialInst);

	//collision
	SphereVisual->SetNotifyRigidBodyCollision(true);
	SphereVisual->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	SphereVisual->OnComponentHit.AddDynamic(this, &ABigAgentActor::OnHit);

	TargetCheckpoint = 0;

}

// Called when the game starts or when spawned
void ABigAgentActor::BeginPlay()
{
	Super::BeginPlay();

	//manual addition of checkpoints
	Checkpoints.Add(FVector(1230, 1800, 170));
	Checkpoints.Add(FVector(650, 1590, 170));
	Checkpoints.Add(FVector(790, 880, 170));
	Checkpoints.Add(FVector(690, -40, 170));
	Checkpoints.Add(FVector(980, 50, 170));
	Checkpoints.Add(FVector(1730, -110, 170));
	Checkpoints.Add(FVector(1390, 690, 170));
	
}

// Called every frame
void ABigAgentActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//face the next location to travel
	FVector WatchDirection = Checkpoints[TargetCheckpoint] - this->GetActorLocation();
	WatchDirection.Z = 0;
	WatchDirection.Normalize();

	FVector Forward = FVector(1, 0, 0);

	float Dot = FVector::DotProduct(Forward, WatchDirection);
	float Det = Forward.X * WatchDirection.Y + Forward.Y * WatchDirection.X;
	float Rad = FMath::Atan2(Det, Dot);
	float Degrees = FMath::RadiansToDegrees(Rad);

	this->SetActorRotation(FRotator(0, Degrees, 180));
	
	//move/lerp to next location
	RootComponent->SetWorldLocation(FMath::Lerp(this->GetActorLocation(), Checkpoints[TargetCheckpoint], DeltaTime * 1));

	if (FVector::Dist(this->GetActorLocation(), Checkpoints[TargetCheckpoint]) < 3.0f)
	{
		TargetCheckpoint++;
		if (TargetCheckpoint > 6)
			TargetCheckpoint = 0;
	}


}

void ABigAgentActor::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		//reset level
		UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
	}
}

