// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Assignment2/Assignment2HUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAssignment2HUD() {}
// Cross Module References
	ASSIGNMENT2_API UClass* Z_Construct_UClass_AAssignment2HUD_NoRegister();
	ASSIGNMENT2_API UClass* Z_Construct_UClass_AAssignment2HUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Assignment2();
// End Cross Module References
	void AAssignment2HUD::StaticRegisterNativesAAssignment2HUD()
	{
	}
	UClass* Z_Construct_UClass_AAssignment2HUD_NoRegister()
	{
		return AAssignment2HUD::StaticClass();
	}
	struct Z_Construct_UClass_AAssignment2HUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAssignment2HUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_Assignment2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAssignment2HUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "Assignment2HUD.h" },
		{ "ModuleRelativePath", "Assignment2HUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAssignment2HUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAssignment2HUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAssignment2HUD_Statics::ClassParams = {
		&AAssignment2HUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AAssignment2HUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAssignment2HUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAssignment2HUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAssignment2HUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAssignment2HUD, 2424188166);
	template<> ASSIGNMENT2_API UClass* StaticClass<AAssignment2HUD>()
	{
		return AAssignment2HUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAssignment2HUD(Z_Construct_UClass_AAssignment2HUD, &AAssignment2HUD::StaticClass, TEXT("/Script/Assignment2"), TEXT("AAssignment2HUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAssignment2HUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
