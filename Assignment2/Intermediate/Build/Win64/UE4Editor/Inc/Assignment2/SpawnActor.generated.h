// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSIGNMENT2_SpawnActor_generated_h
#error "SpawnActor.generated.h already included, missing '#pragma once' in SpawnActor.h"
#endif
#define ASSIGNMENT2_SpawnActor_generated_h

#define Assignment2_Source_Assignment2_SpawnActor_h_16_SPARSE_DATA
#define Assignment2_Source_Assignment2_SpawnActor_h_16_RPC_WRAPPERS
#define Assignment2_Source_Assignment2_SpawnActor_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Assignment2_Source_Assignment2_SpawnActor_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawnActor(); \
	friend struct Z_Construct_UClass_ASpawnActor_Statics; \
public: \
	DECLARE_CLASS(ASpawnActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment2"), NO_API) \
	DECLARE_SERIALIZER(ASpawnActor)


#define Assignment2_Source_Assignment2_SpawnActor_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASpawnActor(); \
	friend struct Z_Construct_UClass_ASpawnActor_Statics; \
public: \
	DECLARE_CLASS(ASpawnActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment2"), NO_API) \
	DECLARE_SERIALIZER(ASpawnActor)


#define Assignment2_Source_Assignment2_SpawnActor_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawnActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawnActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnActor(ASpawnActor&&); \
	NO_API ASpawnActor(const ASpawnActor&); \
public:


#define Assignment2_Source_Assignment2_SpawnActor_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnActor(ASpawnActor&&); \
	NO_API ASpawnActor(const ASpawnActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawnActor)


#define Assignment2_Source_Assignment2_SpawnActor_h_16_PRIVATE_PROPERTY_OFFSET
#define Assignment2_Source_Assignment2_SpawnActor_h_13_PROLOG
#define Assignment2_Source_Assignment2_SpawnActor_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment2_Source_Assignment2_SpawnActor_h_16_PRIVATE_PROPERTY_OFFSET \
	Assignment2_Source_Assignment2_SpawnActor_h_16_SPARSE_DATA \
	Assignment2_Source_Assignment2_SpawnActor_h_16_RPC_WRAPPERS \
	Assignment2_Source_Assignment2_SpawnActor_h_16_INCLASS \
	Assignment2_Source_Assignment2_SpawnActor_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment2_Source_Assignment2_SpawnActor_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment2_Source_Assignment2_SpawnActor_h_16_PRIVATE_PROPERTY_OFFSET \
	Assignment2_Source_Assignment2_SpawnActor_h_16_SPARSE_DATA \
	Assignment2_Source_Assignment2_SpawnActor_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment2_Source_Assignment2_SpawnActor_h_16_INCLASS_NO_PURE_DECLS \
	Assignment2_Source_Assignment2_SpawnActor_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT2_API UClass* StaticClass<class ASpawnActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment2_Source_Assignment2_SpawnActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
