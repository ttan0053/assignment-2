// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Assignment2/SpawnActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpawnActor() {}
// Cross Module References
	ASSIGNMENT2_API UClass* Z_Construct_UClass_ASpawnActor_NoRegister();
	ASSIGNMENT2_API UClass* Z_Construct_UClass_ASpawnActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Assignment2();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void ASpawnActor::StaticRegisterNativesASpawnActor()
	{
	}
	UClass* Z_Construct_UClass_ASpawnActor_NoRegister()
	{
		return ASpawnActor::StaticClass();
	}
	struct Z_Construct_UClass_ASpawnActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpawnInterval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StartTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumberBigAgents_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MaxNumberBigAgents;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumberSmallAgents_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MaxNumberSmallAgents;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnObject1_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SpawnObject1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnObject2_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SpawnObject2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnObject3_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SpawnObject3;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpawnActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Assignment2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SpawnActor.h" },
		{ "ModuleRelativePath", "SpawnActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnInterval_MetaData[] = {
		{ "Category", "SpawnActor" },
		{ "ModuleRelativePath", "SpawnActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnInterval = { "SpawnInterval", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnActor, SpawnInterval), METADATA_PARAMS(Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnInterval_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnInterval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnActor_Statics::NewProp_StartTime_MetaData[] = {
		{ "Category", "SpawnActor" },
		{ "ModuleRelativePath", "SpawnActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ASpawnActor_Statics::NewProp_StartTime = { "StartTime", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnActor, StartTime), METADATA_PARAMS(Z_Construct_UClass_ASpawnActor_Statics::NewProp_StartTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnActor_Statics::NewProp_StartTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnActor_Statics::NewProp_MaxNumberBigAgents_MetaData[] = {
		{ "Category", "SpawnActor" },
		{ "ModuleRelativePath", "SpawnActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ASpawnActor_Statics::NewProp_MaxNumberBigAgents = { "MaxNumberBigAgents", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnActor, MaxNumberBigAgents), METADATA_PARAMS(Z_Construct_UClass_ASpawnActor_Statics::NewProp_MaxNumberBigAgents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnActor_Statics::NewProp_MaxNumberBigAgents_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnActor_Statics::NewProp_MaxNumberSmallAgents_MetaData[] = {
		{ "Category", "SpawnActor" },
		{ "ModuleRelativePath", "SpawnActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ASpawnActor_Statics::NewProp_MaxNumberSmallAgents = { "MaxNumberSmallAgents", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnActor, MaxNumberSmallAgents), METADATA_PARAMS(Z_Construct_UClass_ASpawnActor_Statics::NewProp_MaxNumberSmallAgents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnActor_Statics::NewProp_MaxNumberSmallAgents_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject1_MetaData[] = {
		{ "Category", "SpawnObject" },
		{ "ModuleRelativePath", "SpawnActor.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject1 = { "SpawnObject1", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnActor, SpawnObject1), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject2_MetaData[] = {
		{ "Category", "SpawnObject" },
		{ "ModuleRelativePath", "SpawnActor.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject2 = { "SpawnObject2", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnActor, SpawnObject2), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject3_MetaData[] = {
		{ "Category", "SpawnObject" },
		{ "ModuleRelativePath", "SpawnActor.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject3 = { "SpawnObject3", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpawnActor, SpawnObject3), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject3_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject3_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASpawnActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnInterval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnActor_Statics::NewProp_StartTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnActor_Statics::NewProp_MaxNumberBigAgents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnActor_Statics::NewProp_MaxNumberSmallAgents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnActor_Statics::NewProp_SpawnObject3,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpawnActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpawnActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASpawnActor_Statics::ClassParams = {
		&ASpawnActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ASpawnActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASpawnActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpawnActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASpawnActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASpawnActor, 3337300712);
	template<> ASSIGNMENT2_API UClass* StaticClass<ASpawnActor>()
	{
		return ASpawnActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASpawnActor(Z_Construct_UClass_ASpawnActor, &ASpawnActor::StaticClass, TEXT("/Script/Assignment2"), TEXT("ASpawnActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpawnActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
