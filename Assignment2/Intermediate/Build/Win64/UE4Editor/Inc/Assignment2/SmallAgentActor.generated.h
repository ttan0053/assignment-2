// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef ASSIGNMENT2_SmallAgentActor_generated_h
#error "SmallAgentActor.generated.h already included, missing '#pragma once' in SmallAgentActor.h"
#endif
#define ASSIGNMENT2_SmallAgentActor_generated_h

#define Assignment2_Source_Assignment2_SmallAgentActor_h_18_SPARSE_DATA
#define Assignment2_Source_Assignment2_SmallAgentActor_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define Assignment2_Source_Assignment2_SmallAgentActor_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define Assignment2_Source_Assignment2_SmallAgentActor_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASmallAgentActor(); \
	friend struct Z_Construct_UClass_ASmallAgentActor_Statics; \
public: \
	DECLARE_CLASS(ASmallAgentActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment2"), NO_API) \
	DECLARE_SERIALIZER(ASmallAgentActor)


#define Assignment2_Source_Assignment2_SmallAgentActor_h_18_INCLASS \
private: \
	static void StaticRegisterNativesASmallAgentActor(); \
	friend struct Z_Construct_UClass_ASmallAgentActor_Statics; \
public: \
	DECLARE_CLASS(ASmallAgentActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment2"), NO_API) \
	DECLARE_SERIALIZER(ASmallAgentActor)


#define Assignment2_Source_Assignment2_SmallAgentActor_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASmallAgentActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASmallAgentActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASmallAgentActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASmallAgentActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASmallAgentActor(ASmallAgentActor&&); \
	NO_API ASmallAgentActor(const ASmallAgentActor&); \
public:


#define Assignment2_Source_Assignment2_SmallAgentActor_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASmallAgentActor(ASmallAgentActor&&); \
	NO_API ASmallAgentActor(const ASmallAgentActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASmallAgentActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASmallAgentActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASmallAgentActor)


#define Assignment2_Source_Assignment2_SmallAgentActor_h_18_PRIVATE_PROPERTY_OFFSET
#define Assignment2_Source_Assignment2_SmallAgentActor_h_15_PROLOG
#define Assignment2_Source_Assignment2_SmallAgentActor_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment2_Source_Assignment2_SmallAgentActor_h_18_PRIVATE_PROPERTY_OFFSET \
	Assignment2_Source_Assignment2_SmallAgentActor_h_18_SPARSE_DATA \
	Assignment2_Source_Assignment2_SmallAgentActor_h_18_RPC_WRAPPERS \
	Assignment2_Source_Assignment2_SmallAgentActor_h_18_INCLASS \
	Assignment2_Source_Assignment2_SmallAgentActor_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment2_Source_Assignment2_SmallAgentActor_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment2_Source_Assignment2_SmallAgentActor_h_18_PRIVATE_PROPERTY_OFFSET \
	Assignment2_Source_Assignment2_SmallAgentActor_h_18_SPARSE_DATA \
	Assignment2_Source_Assignment2_SmallAgentActor_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment2_Source_Assignment2_SmallAgentActor_h_18_INCLASS_NO_PURE_DECLS \
	Assignment2_Source_Assignment2_SmallAgentActor_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT2_API UClass* StaticClass<class ASmallAgentActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment2_Source_Assignment2_SmallAgentActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
