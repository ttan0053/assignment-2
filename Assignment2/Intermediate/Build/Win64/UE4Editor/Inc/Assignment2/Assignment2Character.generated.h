// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSIGNMENT2_Assignment2Character_generated_h
#error "Assignment2Character.generated.h already included, missing '#pragma once' in Assignment2Character.h"
#endif
#define ASSIGNMENT2_Assignment2Character_generated_h

#define Assignment2_Source_Assignment2_Assignment2Character_h_21_SPARSE_DATA
#define Assignment2_Source_Assignment2_Assignment2Character_h_21_RPC_WRAPPERS
#define Assignment2_Source_Assignment2_Assignment2Character_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Assignment2_Source_Assignment2_Assignment2Character_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAssignment2Character(); \
	friend struct Z_Construct_UClass_AAssignment2Character_Statics; \
public: \
	DECLARE_CLASS(AAssignment2Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment2"), NO_API) \
	DECLARE_SERIALIZER(AAssignment2Character)


#define Assignment2_Source_Assignment2_Assignment2Character_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAAssignment2Character(); \
	friend struct Z_Construct_UClass_AAssignment2Character_Statics; \
public: \
	DECLARE_CLASS(AAssignment2Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Assignment2"), NO_API) \
	DECLARE_SERIALIZER(AAssignment2Character)


#define Assignment2_Source_Assignment2_Assignment2Character_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAssignment2Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAssignment2Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAssignment2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment2Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAssignment2Character(AAssignment2Character&&); \
	NO_API AAssignment2Character(const AAssignment2Character&); \
public:


#define Assignment2_Source_Assignment2_Assignment2Character_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAssignment2Character(AAssignment2Character&&); \
	NO_API AAssignment2Character(const AAssignment2Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAssignment2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAssignment2Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAssignment2Character)


#define Assignment2_Source_Assignment2_Assignment2Character_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AAssignment2Character, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AAssignment2Character, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AAssignment2Character, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AAssignment2Character, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AAssignment2Character, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AAssignment2Character, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AAssignment2Character, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AAssignment2Character, L_MotionController); }


#define Assignment2_Source_Assignment2_Assignment2Character_h_18_PROLOG
#define Assignment2_Source_Assignment2_Assignment2Character_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment2_Source_Assignment2_Assignment2Character_h_21_PRIVATE_PROPERTY_OFFSET \
	Assignment2_Source_Assignment2_Assignment2Character_h_21_SPARSE_DATA \
	Assignment2_Source_Assignment2_Assignment2Character_h_21_RPC_WRAPPERS \
	Assignment2_Source_Assignment2_Assignment2Character_h_21_INCLASS \
	Assignment2_Source_Assignment2_Assignment2Character_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Assignment2_Source_Assignment2_Assignment2Character_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Assignment2_Source_Assignment2_Assignment2Character_h_21_PRIVATE_PROPERTY_OFFSET \
	Assignment2_Source_Assignment2_Assignment2Character_h_21_SPARSE_DATA \
	Assignment2_Source_Assignment2_Assignment2Character_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Assignment2_Source_Assignment2_Assignment2Character_h_21_INCLASS_NO_PURE_DECLS \
	Assignment2_Source_Assignment2_Assignment2Character_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT2_API UClass* StaticClass<class AAssignment2Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Assignment2_Source_Assignment2_Assignment2Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
